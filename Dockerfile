FROM python:3.6-alpine

COPY app.py /root/

RUN chmod u+x app.py

CMD ["python3", "/root/app.py"]

